﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private readonly SetPartnerPromoCodeLimitRequest _request;

        public SetPartnerPromoCodeLimitRequestBuilder()
        {
            _request = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Now,
                Limit = 0
            };
        }

        public SetPartnerPromoCodeLimitRequestBuilder AddEndDate(DateTime date)
        {
            _request.EndDate = date;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder AddLimit(int limit)
        {
            _request.Limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return _request;
        }
    }
}
