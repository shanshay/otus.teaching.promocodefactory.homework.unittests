﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly int _randomInt;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            _randomInt = new Random().Next(1, 100);
        }


        // Если партнер не найден, то также нужно выдать ошибку 404
        // 
        [Fact]
        public async System.Threading.Tasks.Task SetPartnerPromoCodeLimitAsync_IfParthnerNotFound_Return404Error()
        {
            //Arrange
            var partner = new PartnerBuilder().Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => null);

            //Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                    new SetPartnerPromoCodeLimitRequest());
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        //Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfParthnerIsBloCked_Return404Error()
        {
            //Arrange
            var partner = new PartnerBuilder().AddIsActive(false).Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                    new SetPartnerPromoCodeLimitRequest());
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется
        [Theory]
        [InlineData(1)]
        [InlineData(20)]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesShouldBeZero(int codes)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(codes)
                .AddPartnerLimits(codes)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(new Random().Next(1, 100))
                .Build();
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(20)]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesShouldNotChanged(int codes)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(codes)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(codes)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(codes);
        }

        //При установке лимита нужно отключить предыдущий лимит
        [Theory]
        [InlineData(1)]
        [InlineData(20)]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitIsEnded_DisablePReviousLimit(int codes)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(codes)
                .AddPartnerLimits(codes)
                .Build();

            var limit = partner.PartnerLimits.First();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(codes)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            limit.CancelDate.Should().HaveValue();
        }

        //Лимит должен быть больше 0

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_LimitShouldBeMoreZero(int limit)
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(1)
                .AddPartnerLimits(1)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(limit)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            var result =
                await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                    new SetPartnerPromoCodeLimitRequest());
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        //Нужно убедиться, что сохранили новый лимит в базу данных
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_RepositoryShouldCallUpdateAsyncOnce()
        {
            //Arrange
            var partner = new PartnerBuilder()
                .AddIsActive(true)
                .AddNumberIssuedPromoCodes(1)
                .AddPartnerLimits(1)
                .Build();

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .AddLimit(10)
                .Build();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);

        }
    }
}