﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private readonly Partner _partner;

        public PartnerBuilder()
        {
            _partner = new Partner
            {
                Id = Guid.NewGuid(),
                NumberIssuedPromoCodes = 0,
                IsActive = false,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }

        public PartnerBuilder AddId(Guid id)
        {
            _partner.Id = id;
            return this;
        }

        public PartnerBuilder AddName(string name)
        {
            _partner.Name = name;
            return this;
        }

        public PartnerBuilder AddIsActive(bool isActive)
        {
            _partner.IsActive = isActive;
            return this;
        }

        public PartnerBuilder AddNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public PartnerBuilder AddPartnerLimits(int limit)
        {
            _partner.PartnerLimits.Add(new PartnerPromoCodeLimit()
            {
                Id = new Guid(),
                Limit = limit
            });
            return this;
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
