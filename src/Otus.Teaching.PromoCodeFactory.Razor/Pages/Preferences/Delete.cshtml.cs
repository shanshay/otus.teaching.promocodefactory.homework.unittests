﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.Razor.Pages.Preferences
{
    public class DeleteModel : PageModel
    {
        private readonly Otus.Teaching.PromoCodeFactory.DataAccess.DataContext _context;

        public DeleteModel(Otus.Teaching.PromoCodeFactory.DataAccess.DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Preference Preference { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Preference = await _context.Preferences.FirstOrDefaultAsync(m => m.Id == id);

            if (Preference == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Preference = await _context.Preferences.FindAsync(id);

            if (Preference != null)
            {
                _context.Preferences.Remove(Preference);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
