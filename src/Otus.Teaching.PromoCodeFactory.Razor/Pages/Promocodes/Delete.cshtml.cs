﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.Razor.Pages.Promocodes
{
    public class DeleteModel : PageModel
    {
        private readonly Otus.Teaching.PromoCodeFactory.DataAccess.DataContext _context;

        public DeleteModel(Otus.Teaching.PromoCodeFactory.DataAccess.DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public PromoCode PromoCode { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            PromoCode = await _context.PromoCodes.FirstOrDefaultAsync(m => m.Id == id);

            if (PromoCode == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            PromoCode = await _context.PromoCodes.FindAsync(id);

            if (PromoCode != null)
            {
                _context.PromoCodes.Remove(PromoCode);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
