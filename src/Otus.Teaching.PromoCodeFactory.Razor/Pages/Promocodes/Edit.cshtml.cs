﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.Razor.Pages.Promocodes
{
    public class EditModel : PageModel
    {
        private readonly Otus.Teaching.PromoCodeFactory.DataAccess.DataContext _context;

        public EditModel(Otus.Teaching.PromoCodeFactory.DataAccess.DataContext context)
        {
            _context = context;
        }

        [BindProperty]
        public PromoCode PromoCode { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            PromoCode = await _context.PromoCodes.FirstOrDefaultAsync(m => m.Id == id);

            if (PromoCode == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(PromoCode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PromoCodeExists(PromoCode.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool PromoCodeExists(Guid id)
        {
            return _context.PromoCodes.Any(e => e.Id == id);
        }
    }
}
